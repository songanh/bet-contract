// SPDX-License-Identifier: MIT
pragma solidity >=0.4.22 <0.9.0;

library Utils {
    function getPrizeAfterFee(
        uint256 prize,
        uint256 betPrice,
        uint8 fee
    ) internal pure returns (uint256) {
        uint256 response = prize - (((prize - betPrice) * fee) / 100);
        return response;
    }
}

contract App {
    address private _owner;
    mapping(address => uint256) private _balances;
    uint256 private _totalSupply;
    uint8 private _decimals;
    string private _symbol;
    string private _name;
    uint256 private _rate = 10000;
    address private _feeHolder;
    address private _editor;

    constructor() {
        _owner = msg.sender;
        _name = "BET TOKEN";
        _symbol = "BET";
        _decimals = 18;
        _totalSupply = 0;
        _balances[msg.sender] = _totalSupply;
        _feeHolder = msg.sender;
        _editor = msg.sender;
    }

    modifier onlyOwner() {
        require(_owner == msg.sender, "Caller is not the owner");
        _;
    }

    modifier onlyEditor() {
        require(_editor == msg.sender, "Caller is not the editor");
        _;
    }

    function getOwner() external view returns (address) {
        return _owner;
    }

    function decimals() external view returns (uint8) {
        return _decimals;
    }

    function symbol() external view returns (string memory) {
        return _symbol;
    }

    function name() external view returns (string memory) {
        return _name;
    }

    function totalSupply() external view returns (uint256) {
        return _totalSupply;
    }

    function balanceOf(address account) external view returns (uint256) {
        return _balances[account];
    }

    function _mint(address account, uint256 amount) private {
        require(account != address(0), "BEP20: mint to the zero address");
        _totalSupply = _totalSupply += amount;
        _balances[account] += amount;
    }

    function _burn(address account, uint256 amount) private {
        require(account != address(0), "BEP20: burn from the zero address");

        _balances[account] -= amount;
        _totalSupply -= amount;
    }

    function buyTokens() external payable {
        _mint(msg.sender, msg.value * _rate);
    }

    function sellTokens(uint256 _amount) external {
        require(_balances[msg.sender] >= _amount);
        uint256 etherAmount = _amount / _rate;
        require(address(this).balance >= etherAmount);
        _burn(msg.sender, _amount);
        payable(msg.sender).transfer(etherAmount);
    }

    function changeOwner(address newOwner) external onlyOwner {
        require(newOwner != address(0), "New owner is the zero address");
        _owner = newOwner;
    }

    function changeEditor(address newEditor) external onlyOwner {
        require(newEditor != address(0), "New editor is the zero address");
        _editor = newEditor;
    }

    function changeFeeHolder(address newFeeHolder) external onlyOwner {
        require(
            newFeeHolder != address(0),
            "New fee holder is the zero address"
        );
        _feeHolder = newFeeHolder;
    }

    function getEditor() external view returns (address) {
        return _editor;
    }

    // bussiness logic
    struct MatchInfo {
        string id;
        uint256 startDate;
        string vongdau;
        string giaidau;
        string team1;
        string team2;
        bool finish;
        bool allowBet;
        uint256 betPrice;
        bool cancel;
        uint8 mode;
        string result;
        uint8 fee;
        string name;
    }

    struct BetInfo {
        string matchId;
        address player;
        string bet; // ex: 1-1, 1-0, 2-1, win, lose, draw ...
    }

    mapping(string => MatchInfo) internal _matchInfoMap;
    string[] internal _ids;
    mapping(string => BetInfo[]) internal _betMap;

    function addMatchInfo(MatchInfo memory matchInfo) external onlyEditor {
        bool exist = false;
        for (uint256 index = 0; index < _ids.length; index++) {
            if (
                keccak256(abi.encodePacked(matchInfo.id)) ==
                keccak256(abi.encodePacked(_ids[index]))
            ) {
                exist = true;
                break;
            }
        }
        if (!exist) {
            _ids.push(matchInfo.id);
            _matchInfoMap[matchInfo.id] = matchInfo;
        }
    }

    function updateResult(string memory id, string memory result)
        external
        onlyEditor
    {
        MatchInfo memory info = _matchInfoMap[id];
        info.result = result;
        _matchInfoMap[id] = info;
    }

    function disableBet(string memory id) external onlyEditor {
        MatchInfo memory info = _matchInfoMap[id];
        info.allowBet = false;
        _matchInfoMap[id] = info;
    }

    function cancelBet(string memory id) external onlyEditor {
        MatchInfo memory info = _matchInfoMap[id];
        info.cancel = true;
        _matchInfoMap[id] = info;
    }

    function finishMatch(string memory id) external onlyEditor {
        MatchInfo memory info = _matchInfoMap[id];
        if (!info.allowBet) {
            uint8 fee = info.fee;
            uint256 betPrice = info.betPrice;
            BetInfo[] storage betList = _betMap[id];
            if (info.cancel) {
                // refund to all player
                for (uint256 index = 0; index < betList.length; index++) {
                    address player = betList[index].player;
                    _mint(player, betPrice);
                }
            } else {
                uint256 winner = 0;
                uint256 totalFund = 0;
                for (uint256 index = 0; index < betList.length; index++) {
                    if (
                        keccak256(abi.encodePacked(betList[index].bet)) ==
                        keccak256(abi.encodePacked(info.result))
                    ) {
                        winner++;
                    }
                    totalFund += betPrice;
                }

                if (winner > 0) {
                    uint256 prize = totalFund / winner;
                    uint256 prizeAfterfee = Utils.getPrizeAfterFee(
                        prize,
                        betPrice,
                        fee
                    );
                    uint256 creatorReward = prize - prizeAfterfee;
                    for (uint256 index = 0; index < betList.length; index++) {
                        if (
                            keccak256(abi.encodePacked(betList[index].bet)) ==
                            keccak256(abi.encodePacked(info.result))
                        ) {
                            address player = betList[index].player;
                            _mint(player, prizeAfterfee);
                            if (creatorReward > 0) {
                                _mint(_feeHolder, creatorReward);
                            }
                        }
                    }
                } else {
                    // refund to all player
                    for (uint256 index = 0; index < betList.length; index++) {
                        address player = betList[index].player;
                        _mint(player, betPrice);
                    }
                }
            }
            info.finish = true;
            _matchInfoMap[id] = info;
        }
    }

    function getMatch(bool active) external view returns (MatchInfo[] memory) {
        uint256 size = 0;
        for (uint256 index = 0; index < _ids.length; index++) {
            MatchInfo memory info = _matchInfoMap[_ids[index]];
            if (active) {
                if (!info.finish) {
                    size++;
                }
            } else {
                if (info.finish) {
                    size++;
                }
            }
        }
        MatchInfo[] memory response = new MatchInfo[](size);
        uint256 j = 0;
        for (uint256 index = 0; index < _ids.length; index++) {
            MatchInfo memory info = _matchInfoMap[_ids[index]];
            if (active) {
                if (!info.finish) {
                    response[j] = info;
                    j++;
                }
            } else {
                if (info.finish) {
                    response[j] = info;
                    j++;
                }
            }
        }
        return response;
    }

    function getBet(string memory id)
        external
        view
        returns (BetInfo memory response)
    {
        BetInfo[] memory betList = _betMap[id];
        for (uint256 index = 0; index < betList.length; index++) {
            if (betList[index].player == msg.sender) {
                response = betList[index];
                break;
            }
        }
    }

    function getBetList(string memory id)
        public
        view
        returns (BetInfo[] memory)
    {
        return _betMap[id];
    }

    function getMatch(string memory id) public view returns (MatchInfo memory) {
        return _matchInfoMap[id];
    }

    function bet(string memory matchId, string memory playerBet) external {
        MatchInfo memory matchInfo = _matchInfoMap[matchId];
        uint256 betPrice = matchInfo.betPrice;
        if (matchInfo.allowBet && _balances[msg.sender] >= betPrice) {
            BetInfo[] storage betList = _betMap[matchId];
            bool exist = false;
            for (uint256 index = 0; index < betList.length; index++) {
                if (betList[index].player == msg.sender) {
                    exist = true;
                    break;
                }
            }
            if (!exist) {
                BetInfo memory betInfo;
                betInfo.matchId = matchId;
                betInfo.player = msg.sender;
                betInfo.bet = playerBet;
                betList.push(betInfo);
                _betMap[matchId] = betList;
                _burn(msg.sender, betPrice);
            }
        }
    }
}
